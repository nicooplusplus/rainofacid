#ifndef UI_H
#define UI_H

#include<iostream>  //ok
#include<ctime> //ok
#include<fstream>
#include<map>
#include<vector>
#include<pthread.h>
#include<X11/Xlib.h>
#include<X11/Xutil.h> // ex : "XFindContext()"
#include<cairo/cairo.h>
#include<cairo/cairo-pdf.h>
#include<cairo/cairo-ps.h>
#include<cairo/cairo-xlib.h>
#include<X11/Xlib.h>

#include<string.h>

/*
#include<ft2build.h>
#include FT_FREETYPE_H
*/

enum UIelementType { FRAME, BUTTON , EXTERNAL };

#define LOG

class UI{
 private:
  UI(){}; //ok, (non utilisé)
  ~UI(){}; //ok, (non utilisé)
  static UI *singleton; //ok

  #ifdef LOG
  std::ofstream logRainOfAcid; //ok
  #endif
  bool aborting = false; //ok
  Display *dpy; //ok
  int scr; //ok
  int dpt; //ok
  Window rootWin;

  pthread_t UIthread;
  pthread_t cursor_thread;
  bool running;
  XContext frame_context;
  XContext button_context;
  XContext button_command_context;
  XPointer pcontext;
  XIM input_manager;
  bool blinking;
  int textSize;
  Window cursor_win;
  bool cursor_on_off;

  struct Element{
    Window xw;
    Pixmap double_buffer;
    void (*p)(void);
    std::string forTextArea;
    cairo_t *c_text;
    cairo_surface_t *s_text;
    double x,y; // position of text cursor
    Window cw; // window used to draw a text cursor
    XIC ic; // pour textarea
    std::vector<int>char_length; // characters length (e.g. "é" = 2) associated to "forTextArea". Used to remove correctly characters
    int size; // size of text
    std::string name; // name of the element
    int width; // width of the xw
    int height; // height of the xw
  };

  std::map<std::string,Element> listOfFrames;
  std::map<Window,Element> listOfButtons;
  std::map<Window,Element> listOfTextAreas;
  std::map<Window,Element> listOfSimpleText;
  std::vector<Element> listOfFrames2;

 public: /* static uniquement pour les méthodes appelées par l'utilisateur! */
         /* utiliser singleton-> pour static, et les autres this->          */
  // user :
  static UI *initialise(void); // ~ constructor // ok
  static void kill();      // ~ deathtructor // ok
  static void run(); // ok
  static void stop(void); // ok
  static bool still_running(void);    // for the main loop // ok
  static void addFrame(std::string,int,int,int,int,std::string = ""); // ok
  static void checkSingleton(bool,int,std::string); // should be inlined ?? // ok

  // internal :
  #ifdef LOG
  void log(std::string); // ok
  void log(std::string,int,std::string); // ok
  std::string timer(void); // ok
  #endif
  void abort(); // ok
  void setInputManager(void); // ok


  // to check :
  Display *getDisplay(void);  
  static void *loop(void *);
  static size_t hexcol(std::string);
  static void addButton(int,int,int,int,std::string,std::string,std::string,void (*)(void));
  void write(Element*);   // last version  
  static void call(Window); // actualy "called" when any mouse button is released
  static void addTextInput(int,int,int,int,std::string,std::string,std::string =""); // pour écrire du texte
  static void key(XEvent *);
  XIM getInputManager(void);
  XIC *getInputContext(Window);
  static void *textCursorBlink(void *); // used to blink the cursor of focused input context
  static void setActiveCursor(Window *); // set the window to blink
  static void setInactiveCursor(Window *); // tell the blinking window to stop
  static void handleEvents(XEvent *);
  static void addSimpleText(int,int,int,int,std::string,unsigned long,std::string,std::string,bool=false /*not selectable by default*/,int =0,unsigned int =0);
  static void SetFont(Element*,int,int); // cette fonction est appelé par "addSimpleText" et "addTextArea" ne doit pas être appelée par l'utilisateur, en écrire une autre pour ça!
  void SimpleTextSelected(Window *);
  void ElementString(Element*,std::string);
  static void GlyphsTest(int,int,int,int,std::string,std::string);
  static void addGlyphInput(int,int,int,int,std::string,std::string,std::string =""); // pour écrire du texte
  static void redrawWindow(XEvent *);
  static void saveWindow(XEvent *);

  // old stuffs
  void write2(Element*); // first version
  static void addFrame2(int,int,int,int,std::string,unsigned long,std::string ="rootwin",int =0,unsigned int =0);

};

#define _DPY_ singleton->dpy //ok
#define _SCR_ singleton->scr //ok
#define _DPT_ singleton->dpt //ok
#define _RTW_ singleton->rootWin //ok
#define _CHECK_SINGLETON_(b) UI::checkSingleton(b,__LINE__,__FILE__) // ok
#define _WIN_(s) singleton->listOfFrames[s].xw // ok
#define _BFR_(s) singleton->listOfFrames[s].double_buffer // ok
#define _FND_(s) (singleton->listOfFrames.find(s) != singleton->listOfFrames.end())


#endif /*UI_H*/
