/*
 *
 * NOTES : 
 *  - il n'est pour l'instant pas très clair sur le nombre de "cairo_t" et "cairo_surface_t" necessaires, ni leur durée de vie.
 *  - lorsqu'on veut afficher un élement de "cairo" dans une "window" de "xlib", il faut que cette dernière soit déjà présente à l'écran.
 *  - pour lancer la commande associée au bouton il faut cliquer et relacher sur le bouton, ainsi on peut eviter de cliquer trop rapidement.
 *  - le boutton exit utilise le meilleur moyen pour quitter, le garder pour exemple.
 *  - actuellement une "string" pour text area est dans "Element" ce qui veut que les "buttons" et les "frames" la posssède aussi
 *    il faudra réfléchir à éventullement séparer les différents élements, ou au moins réfléchir à ce qui peut être fait.
 *  - livre : http://lipn.univ-paris13.fr/~recanati/livre.htm
 *  - la manière dont on précise les locales n'est pas claire, il faut faire des recherches, mais "setlocale()" n'est pas une fonction
 *    de xlib mais de ???
 *  - super cool method to remove the last character of a string : "popback()", nécessite d'ajouter "-std=c++11" à "g++" dans le 
 *    "Makefile"
 *  - Avec Xlib pas plus d'un thread!
 *  - réécriture de la gestion des events "UI::loop()" et "UI::HandleEvents()" à l'aide de twm (voir "HandleEvents()" dans event.c
 *  - from cairo web site : "Be aware cairo_show_text() caches glyphs so is much more efficient if you work with a lot of text"
 *  - manipulation du système de fichier : http://www.gnu.org/software/libc/manual/html_node/index.html#Top
 *  - structure de données pour les fichiers dans "/usr/include/bits/stat.h"
 *  - actuellement il faut distinguer les "window" qui sont sucesptibles de voir leur contenu changer, de celles qui sont fixes
 *    comme les boutons. De fait, les boutons auront donc besoin de copier qu'au moment de leur création leur contenu dans un
 *    "pixmap".
 *  - voir si il y a moyen d'empêcher l'appel de certaines méthodes. car actuellement en récupérant l'adresse du singleton on peut
 *    appeller des méthodes non "static".
 *  - death said : "I'd rather kill myself than using another ui!"
 *  - pour documentation : une frame peut contenir d'autres frames, ou tout autre élement, mais aucun autre élement peut contenir
 *    de frame.
 * 
 *
 *  ->>>>> vérifier que les log soient bien entre des ifdef
 */

#include"UI.h"
#include<iostream>
#include<unistd.h>
#include <dirent.h> // "scandir()" 
#include <sys/stat.h> // reading attributs from files : http://www.gnu.org/software/libc/manual/html_node/Reading-Attributes.html#Reading-Attributes

using namespace std;

#define WIDTH  1200
#define HEIGHT 512
#define WMOTHER 1200
#define HMOTHER 568
#define BKG1   UI::hexcol("#141313") // fond de la fenêtre application
#define BRD1   UI::hexcol("#3c3b37")  
#define BKG2   UI::hexcol("#3c3b37") // panels
#define BKG3   UI::hexcol("#d94a38") // processing field
#define BKG4   UI::hexcol("#2f2e2c") // pointer position field
#define BLCK   0
#define COL1   UI::hexcol("#777777")
#define TITLE "RainOfAcid-0.0.5"


int main()
{
  // on démare l'interface
  UI *toto = UI::initialise(); // doit être avant tout appel de xlib à cause du trheading
  UI::addFrame("toto",100,100,100,100,"");
  UI::addFrame("toto2",10,10,10,10,"toto");

  // note : tjrs prendre soin d'avoir le bon nom dans le paramètre parent.
  UI::addFrame2(0,0,WMOTHER,HMOTHER,TITLE,BKG1);                   // main window
    UI::addTextInput(20,100,412,24,"df->clp2",TITLE,"test");      // text input for test
      UI::addButton(3,2,50,18,"df->pt:b1",TITLE,"Exit",UI::stop); // exit button

  // run
  UI::run();

  while( UI::still_running() ){
    sleep(1);
    cout << "heartbeat\n";
  }

  // DEATHSTRUCTION
  UI::kill();

  cout << "good bye.\n";
}
