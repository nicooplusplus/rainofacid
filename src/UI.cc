#include"UI.h"
#include<assert.h>
#include<unistd.h> // sleep



using namespace std;

UI *UI::singleton=NULL; // necessaire sinon erreur de link...

UI *UI::initialise() {
  assert(!singleton /*already started*/);
  singleton = new UI;

  #ifdef LOG
  int line;
  singleton->logRainOfAcid.open("logRainOfAcid", ofstream::out | ofstream::trunc), line = __LINE__;
  if (singleton->logRainOfAcid) {
    singleton->logRainOfAcid << "*********************************************************\n**                logRainOfAcid started                **\n** [ error = (EE) ][ success = (SS)]                   **\n** date and time of compilation: " << __DATE__ << ", " << __TIME__ << " **\n*********************************************************\n\n";
    singleton->log("RainOfAcid started!");
  } else {
    cout << "(EE) Unable to create the log, at (" << line << ") in ["<< __FILE__ <<"]" << endl;
    // don't use abort() because here the log isn't initate properly
    singleton->aborting = true;
    singleton->kill();
  }
  #endif

  // OBLIGATOIRE pour faire du threading, doit être avant toute fonction de xlib
  // Attention, un seul thread en même temps...
  #ifdef LOG
  line = __LINE__  + 2;
  #endif
  if(XInitThreads() ){ 
    #ifdef LOG
    singleton->log("(SS) XInitThreads started!");
    #endif
  } else {
    #ifdef LOG
    singleton->log("(EE) XInitThreads failed! is this system support threads ?",line,__FILE__);
    #endif
    singleton->abort();
  }

  // Xlib - display
  #ifdef LOG
  line = __LINE__  + 2;
  #endif
  if (( _DPY_ = XOpenDisplay(NULL)) == NULL) {
    #ifdef LOG
    singleton->log("(EE) XOpenDisplay failed!",line,__FILE__);
    #endif
    singleton->abort();
  }

  // Xlib - select the default screen
  _SCR_ = DefaultScreen(_DPY_);
  
  // Xlib - number of colors
  _DPT_ = DefaultDepth(_DPY_,_SCR_);

  #ifdef LOG
  singleton->log("screen(" + to_string(_SCR_) +") depth(" + to_string(_DPT_) +")");
  #endif

  // set the root window
  _RTW_ = DefaultRootWindow(_DPY_);
  // #01# :
  singleton->listOfFrames["rootwin"].xw = DefaultRootWindow(_DPY_);

  // Xlib - X input manager
  singleton->setInputManager();

  return singleton;
}

Display *UI::getDisplay(){
  return dpy;
}

void UI::kill() {
  assert(singleton);
  if(!singleton->aborting){ // usual close
    #ifdef LOG
    singleton->log("normal exiting, we gonna close the log and delete the singleton");
    singleton->logRainOfAcid.close();
    #endif
    delete singleton;
    singleton = NULL;
  } else { // abort, you know... sometime shit happened...
    #ifdef LOG
    singleton->log(" -=ABORTING=- we gonna close the log and delete the singleton");
    singleton->logRainOfAcid.close();
    #endif
    delete singleton;
    singleton = NULL;
    cout << "program aborted!\n";
    exit(-1);
  }
}

void UI::run() {
  #ifdef LOG
  #endif
  singleton->running = true;
  // boucle principale.
  int status;
  #ifdef LOG
  int line = __LINE__  + 2;
  #endif
  status = pthread_create(&singleton->UIthread,NULL,singleton->loop, NULL);
  if (status == 0) {
    #ifdef LOG
    singleton->log("(SS) the event listener is running");
    #endif
  } else {
    #ifdef LOG
    singleton->log("(EE) the event listener failed to initate his thread",line,__FILE__);
    #endif
    singleton->abort();
  }
}

void *UI::loop(void *arg) {

  Display *dpy = _DPY_;
  XEvent e;

  while(singleton->running) {
  usleep(100);

    // manage the text cursor
    if(singleton->blinking){
      switch(singleton->cursor_on_off){
        case true :  XMapWindow(dpy,singleton->cursor_win); break;
        case false : XUnmapWindow(dpy,singleton->cursor_win); break;    
      }
    }

    // execute event if present
    if(XPending(dpy)>0){
      XNextEvent(dpy, &e);
      if(e.type>=0 && e.type<LASTEvent){
        singleton->handleEvents(&e);
      }
    }
  }

  pthread_exit(NULL); // needed by pthread
  #ifdef LOG
  singleton->log("(SS) the event listener has been stopped");
  #endif
}

void UI::stop() {
  singleton->running=false;
  #ifdef LOG
  singleton->log("order to stop the program sended");
  #endif
}

bool UI::still_running() {
  return singleton->running;
}

/** 
 * addFrame() (external)
 * @_s1: name of the new frame. Can't be "", or an error will occured and the program will abort.
 * @_s2: name of the parent frame. If not set, or if set to "" (no name), means the window of the new frame will be child of the rootwin.
 *
 * this method is used to create a new frame.
 **/
void UI::addFrame(string _s1, int _x, int _y, int _w, int _h, string _s2){
  _CHECK_SINGLETON_(true);
  if ( _FND_(_s1) ) {
    #ifdef LOG
    int line = __LINE__  - 2;
    singleton->log("(EE) the name of the new frame \"" + _s1 + "\" is already used",line,__FILE__);
    #endif
    singleton->abort();    
  }

  if (_s1 == "") {
    #ifdef LOG
    int line = __LINE__  - 2;
    singleton->log("(EE) the name of the new frame can't be \"\"",line,__FILE__);
    #endif
    singleton->abort();
  }

  Window w = XCreateSimpleWindow(_DPY_,(_s2=="") ? _RTW_ : _WIN_(_s2) ,_x,_y,_w,_h,0,0,0);
  XSelectInput(_DPY_, w, ExposureMask|ButtonPressMask|KeyReleaseMask|ButtonReleaseMask|SubstructureNotifyMask);
  XMapRaised(_DPY_,w);
  _WIN_(_s1) = w;

  // double buffer
  GC gc = XCreateGC (_DPY_, w, 0, NULL);
  _BFR_(_s1) = XCreatePixmap(_DPY_, w, _w, _h, _DPT_);
  XCopyArea(_DPY_, w, _BFR_(_s1), gc, 0, 0, _w, _h, 0, 0);
  XFreeGC(_DPY_,gc);
}

void UI::addFrame2(int _x, int _y, int _w, int _h, string _s , unsigned long _bg, string _s2 /* UIelement parent*/,int _bw /*border width*/,
                          unsigned int _bbg /*border background*/ ){
  assert(singleton);  // Assertion `singleton' failed...
  assert(singleton->listOfFrames.find(_s)==singleton->listOfFrames.end()); // if exist, exit the program
  Display *dpy = singleton->getDisplay();
  Window w = XCreateSimpleWindow(dpy,singleton->listOfFrames[_s2].xw,_x,_y,_w,_h,_bw,_bbg,_bg);
  XSelectInput(dpy, w, ExposureMask|ButtonPressMask|KeyReleaseMask|ButtonReleaseMask|SubstructureNotifyMask);
  XSaveContext(dpy, w, singleton->frame_context, singleton->pcontext);
  XMapRaised(dpy,w); // voir la doc française sur les fenêtres
  singleton->listOfFrames[_s].xw = w;
  singleton->listOfFrames[_s].name = _s;

  // pour le double buffer
  GC g = XCreateGC (dpy, w, 0, NULL);
  singleton->listOfFrames[_s].double_buffer = XCreatePixmap(dpy, w, _w, _h, 24 /* depth, mis à l'arrache*/);
  XCopyArea(dpy, w, singleton->listOfFrames[_s].double_buffer, g, 0, 0, _w, _h, 0, 0);
}

size_t UI::hexcol(string _c){
  XColor xc;
  Display *dpy = singleton->getDisplay();

  XParseColor(dpy,DefaultColormap(dpy,0),_c.c_str(),&xc);
  XAllocColor(dpy,DefaultColormap(dpy,0),&xc);

  return xc.pixel;
}

void UI::addButton(int _x, int _y, int _w, int _h, string _s , string _s2/* UIelement parent*/, string _s3/*bouton's caption*/,void (*_p)(void)){
  assert(singleton);  // Assertion `singleton' failed...
//  assert(singleton->listOfButtons.find(_s)==singleton->listOfButtons.end()); // if exist, exit the program
  assert(!(singleton->listOfFrames.find(_s2)==singleton->listOfFrames.end())); // need to check if frame parent exist
  Display *dpy = singleton->getDisplay();
  Window w = XCreateSimpleWindow(dpy,singleton->listOfFrames[_s2].xw,_x,_y,_w,_h,0,0,0);
  XSelectInput(dpy, w, ExposureMask|ButtonPressMask|KeyReleaseMask|ButtonReleaseMask|SubstructureNotifyMask);
  XSaveContext(dpy, w, singleton->button_context, singleton->pcontext); // saves the window adress, for what use ??
  XMapRaised(dpy,w); // voir la doc française sur les fenêtres

  cairo_surface_t *s = cairo_xlib_surface_create(dpy, w, DefaultVisual(dpy, 0), _w, _h);
  cairo_t *c = cairo_create (s);
  cairo_pattern_t *pat;

  cairo_rectangle(c, 0.0, 0.0, _w, _h);
  cairo_set_source_rgb(c, 0, 0, 0);
  cairo_fill(c);

  cairo_select_font_face (c, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
  cairo_set_font_size (c, _h-4);
  cairo_move_to (c, 4.0, _h-4);
  cairo_text_path (c, _s3.c_str());
  cairo_set_source_rgb (c, 1.0, 1.0, 1);
  cairo_fill_preserve (c);
  cairo_show_page(c);
  cairo_destroy(c);

  singleton->listOfButtons[w].xw = w;
  singleton->listOfButtons[w].p = _p; // for find the pointer to the command when clicked
  singleton->listOfButtons[w].name = _s;

  // pour le double buffer
  GC g = XCreateGC (dpy, w, 0, NULL);
  singleton->listOfButtons[w].double_buffer = XCreatePixmap(dpy, w, _w, _h, 24 /* depth, mis à l'arrache*/);
  XCopyArea(dpy, w, singleton->listOfButtons[w].double_buffer, g, 0, 0, _w, _h, 0, 0);
  singleton->listOfButtons[w].width = _w;
  singleton->listOfButtons[w].height = _h;
}

void UI::write2(Element *_el){
  cairo_set_source_rgb (_el->c_text, 0.0, 0.0, 0.0);
  cairo_paint(_el->c_text);
  cairo_move_to (_el->c_text, 4.0, _el->size - _el->y);
  cairo_set_source_rgb (_el->c_text, 1.0, 1.0, 1.0);
  cairo_show_text(_el->c_text, _el->forTextArea.c_str());
  cairo_get_current_point(_el->c_text,&_el->x,0);
//  XMoveWindow(singleton->getDisplay(),singleton->cursor_win,_el->x, _el->y);  // move the cursor, need cairo position!
}

void UI::write(Element *_el){ // HERE HERE HERE HERE HERE HERE HERE HERE HERE
  cairo_set_source_rgb (_el->c_text, 0.0, 0.0, 0.0);
  cairo_paint(_el->c_text);
  cairo_move_to (_el->c_text, 4.0, _el->size - _el->y);
  cairo_set_source_rgb (_el->c_text, 1.0, 1.0, 1.0);
  Display *dpy = singleton->getDisplay();
  GC g = XCreateGC (dpy, _el->xw, 0, NULL);
  int i;
  for(i=0;i<_el->forTextArea.length();i++){
    char *c;
    string s;
    //c = _el->forTextArea[i];
    s = _el->forTextArea[i];
    c = (char*)s.c_str();
    while( (*c & 0xC0) == 0x80){ 
    i++;
    s.push_back(_el->forTextArea[i]);
//      cairo_get_current_point(_el->c_text,&_el->x,0);
    }
      cairo_show_text(_el->c_text, c );
/*
    }else{
    }*/
//    *c = 'r';//_el->forTextArea.at(i);
    
  }


  Window w = _el->xw;
  Pixmap p = _el->double_buffer;
  int wd = _el->width;
  int ht = _el->height;
  cout << "h="<<ht<<" w="<<wd<<endl;
  if(singleton->listOfTextAreas.find(_el->xw)!=singleton->listOfTextAreas.end() ){
    cout << "time to save..."<<endl;
    XCopyArea(dpy, w, p, g, 0, 0, wd, ht, 0, 0);
    cout << "...xcopyarea() done!\n";
  }


  if(singleton->cursor_win)  XMoveWindow(singleton->getDisplay(),singleton->cursor_win,_el->x, _el->y);  // move the cursor, need cairo position!
}

void UI::call(Window _w){
  if( singleton->listOfButtons.find(_w)!=singleton->listOfButtons.end() ){
    singleton->listOfButtons[_w].p();
    return;
  } else if ( singleton->listOfTextAreas.find(_w)!=singleton->listOfTextAreas.end() ) {
    cout << "zone de texte\n"; // juste pour afficher dans la console, comme ça on sait quand on clique sur "une zone de texte"
    return;
  }
}

void UI::addTextInput(int _x, int _y, int _w, int _h, string _s /*name*/, string _s2 /*parent name*/, string _s3 /*something to show*/){
  assert(singleton);  // Assertion `singleton' failed...
//  assert(singleton->listOfTextAreas.find(_s)==singleton->listOfTextAreas.end()); // if exist, exit the program
// check if parent exist...

  Display *dpy = singleton->getDisplay();
  Window w = XCreateSimpleWindow(dpy,singleton->listOfFrames[_s2].xw,_x,_y,_w,_h,0,0,0);
  XSelectInput(dpy, w, ExposureMask|ButtonPressMask|KeyPressMask|KeyReleaseMask|ButtonReleaseMask|SubstructureNotifyMask|EnterWindowMask|LeaveWindowMask);
  XMapRaised(dpy,w); // voir la doc française sur les fenêtres
  singleton->listOfTextAreas[w].xw = w;
  singleton->listOfTextAreas[w].name = _s;

  // "style" stuffs come from example 11-14
  int i;
  XIC ic;
  XIMStyles *im_supported_styles;
  XIMStyle app_supported_styles;
  XIMStyle best_style;
  XIMStyle style;

  // 11-14 // set flags for the styles our application can support
  app_supported_styles  = XIMPreeditNone | XIMPreeditNothing | XIMPreeditArea;
  app_supported_styles |= XIMStatusNone  | XIMStatusNothing  | XIMStatusArea;

  // 11-14 // figure out wich styles the IM can support
  XGetIMValues(singleton->getInputManager(),XNQueryInputStyle, &im_supported_styles, NULL);
  for(i=0; i<im_supported_styles->count_styles;i++){
    style = im_supported_styles->supported_styles[i];
    
    if( (style & app_supported_styles) == style){
       /// the fucking long function to best choose...
    }
  }


  // next line is from xterm : "charproc.c"
  ic = XCreateIC(singleton->getInputManager(), XNInputStyle, style /*best_style in 11-14*/ /*input_style in "charproc.c"*/, XNClientWindow, w, XNFocusWindow, w, (void *) 0);
  XFree(im_supported_styles);
  if(ic==NULL){
    cout << "could not create an input context\n";
    exit(1);
  }
  // fin pour xlib 11-14

  singleton->SetFont(&singleton->listOfTextAreas[w],_w,_h);
  // ce qui suit doit être retiré un jour ou l'autre..
  int x = 4; int y = 4;

  singleton->listOfTextAreas[w].ic = ic;
  singleton->listOfTextAreas[w].forTextArea = _s3; // chaine de caractère initiale

  // now we'll add the cursor text window
  Window cw = XCreateSimpleWindow(dpy,w,x,y,1,_h-4-1,0,0,UI::hexcol("#ffffff"));
  XSelectInput(dpy, cw, ExposureMask|ButtonPressMask|KeyPressMask|KeyReleaseMask|ButtonReleaseMask|SubstructureNotifyMask);
  //XMapRaised(dpy,cw); // voir la doc française sur les fenêtres
  singleton->listOfTextAreas[w].cw = cw;

  singleton->listOfTextAreas[w].width=_w;
  singleton->listOfTextAreas[w].height=_h;

  singleton->listOfTextAreas[w].double_buffer = XCreatePixmap(dpy, w, _w, _h, 24 /* depth, mis à l'arrache*/);
  
  if(_s3.size()>0) singleton->write(&singleton->listOfTextAreas[w]); // on affiche la chaine de caractère initiale
//  singleton->cursor_win = cw;

  // pour le double buffer

  GC g = XCreateGC (dpy, w, 0, NULL);
//  singleton->listOfTextAreas[w].double_buffer = XCreatePixmap(dpy, w, _w, _h, 24 /* depth, mis à l'arrache*/);
  XCopyArea(dpy, w, singleton->listOfTextAreas[w].double_buffer, g, 0, 0, _w, _h, 0, 0);
//  singleton->listOfTextAreas[w].width=_w;
//  singleton->listOfTextAreas[w].height=_h;
};

void UI::key(XEvent *_e){
  XIC xim_ic = *singleton->getInputContext(_e->xany.window);
  XKeyPressedEvent *kpe = &(_e->xkey);
  string *s = &singleton->listOfTextAreas[_e->xany.window].forTextArea;
  static char lbuf[3];

  if (xim_ic != NULL) {
   XSetICFocus(xim_ic);
   KeySym keysym;
   Status status;
   int i, len;
   int x_old, y_old; // keep old position of the cursor
   vector<int> *char_len = &singleton->listOfTextAreas[_e->xany.window].char_length;

   len = XmbLookupString(xim_ic, kpe, &lbuf[0], 3, &keysym, &status);

   if (status == XBufferOverflow) cout << "buffer overflow 1 (XmbLookupString)\n";


   switch(keysym){
     // voir dans "keysymdef.h" pour connaître les raccourcis associés aux touches
     case XK_BackSpace :
       while(s->size()>0){
         char c = s->back();
         s->pop_back();
         if( (c & 0xC0) != 0x80){
           break;
         }
       }
       break;
     case XK_Return :
         cout << "return\n";
       break;
     case XK_Escape :
         cout << "escape\n";
       break;
     default :
       for(i=0; i<len;i++) s->push_back(lbuf[i]);
       char_len->push_back(len);
       break;
   }
   singleton->write(&singleton->listOfTextAreas[_e->xany.window]);

 }else{
  cout << "no ic ??? it should be here!\n"; // normalement pas besoin de ça... voir "addTextInput()"
 }
};

void UI::setInputManager(){
  struct lconv * lc;
  //cout << "Locale is: " << setlocale(LC_ALL,NULL) << endl;

  #ifdef LOG
  int line = __LINE__+2;
  #endif
  if(setlocale(LC_ALL,"") == NULL) {
    #ifdef LOG
    singleton->log("(EE) cannot set locale",line,__FILE__);
    #endif
    singleton->abort();

  }

  #ifdef LOG
  line = __LINE__+2;
  #endif
  if(!XSupportsLocale()){
    #ifdef LOG
    singleton->log("(EE) X does not support locale",line,__FILE__);
    #endif
    singleton->abort();
  }

  #ifdef LOG
  line = __LINE__+2;
  #endif
  if(XSetLocaleModifiers("") == NULL){
    #ifdef LOG
    singleton->log("(EE) cannot set locale modifiers",line,__FILE__);
    #endif
    singleton->abort();
  }

  #ifdef LOG
  line = __LINE__+2;
  #endif
  if( (this->input_manager = XOpenIM(singleton->getDisplay(), NULL, NULL, NULL)) == NULL){
    #ifdef LOG
    singleton->log("(EE) couldn't open input manager",line,__FILE__);
    #endif
    singleton->abort();
  }

  #ifdef LOG
  singleton->log("Locale (" + (string)setlocale(LC_ALL,NULL) + ")");
  singleton->log("(SS) Input Manager ready!");
  #endif
};

XIM UI::getInputManager(){
  return this->input_manager;
};

XIC *UI::getInputContext(Window _w){
  return  &this->listOfTextAreas[_w].ic;
};

void *UI::textCursorBlink(void *arg){
/*
  Window w = singleton->listOfTextAreas[0].xw;

  Window cw = XCreateSimpleWindow(singleton->getDisplay(),w,100,100,100,100,0,0,UI::hexcol("#ffffff"));
  XSelectInput(singleton->getDisplay(), cw, ExposureMask);
  XMapWindow(singleton->getDisplay(),cw); // voir la doc française sur les fenêtres
*/
  
  while(singleton->running && singleton->blinking) {
    sleep(1);
    if(singleton->cursor_on_off){
      singleton->cursor_on_off = false;
    }else{
      singleton->cursor_on_off = true;
    }
//    if(cw != 0){
//      XMapWindow(singleton->getDisplay(),*win);
  //    cout << "blink bitch... blink!\n";
  //    XUnmapWindow(singleton->getDisplay(),*win);
    //}
  }

  pthread_exit(NULL); // necessaire pour pthread
};

void UI::setActiveCursor(Window *_w){
  if(singleton->listOfTextAreas.find(*_w)!=singleton->listOfTextAreas.end() ){
    // "ActiveCursor" = singleton->listOfTextAreas[*_w].cw;
    cout << "someone has to blink\n";
    // clignotement du curseur.
    singleton->blinking = true;
    singleton->cursor_on_off = true;
    singleton->cursor_win = singleton->listOfTextAreas[*_w].cw;
    XMoveWindow(singleton->getDisplay(),singleton->cursor_win,singleton->listOfTextAreas[*_w].x, singleton->listOfTextAreas[*_w].y);
    pthread_create(&singleton->cursor_thread,NULL,singleton->textCursorBlink, (void *)&  singleton->listOfTextAreas[*_w].cw);
  }
};


void UI::setInactiveCursor(Window *_w){
  if(singleton->listOfTextAreas.find(*_w)!=singleton->listOfTextAreas.end() ){
    cout << "stop blinking bitch!\n";  
    singleton->blinking = false;
    singleton->cursor_on_off = false;
    XUnmapWindow(singleton->getDisplay(),singleton->cursor_win);
  }
};

void UI::handleEvents(XEvent *_e){
Display *dpy = singleton->getDisplay();
    switch(_e->type){
     case MapNotify        :
     case Expose           : redrawWindow(_e); XFlush(dpy); break;
     case MotionNotify     : break;
     case KeyPress         : key(_e); break;
     case KeyRelease       : break;
     case ButtonPress      : /*singleton->selectable(&_e->xany.window)*/; break;
     case ButtonRelease    : call(_e->xany.window); break;
     case EnterNotify      : setActiveCursor(&_e->xany.window); break;
     case LeaveNotify      : setInactiveCursor(&_e->xany.window); break;
     case NoExpose         : cout << "NoExpose\n"; /*saveWindow(_e);*/ break;
     case DestroyNotify    : cout << "DestroyNotify\n"; break;
     case VisibilityNotify : cout << "VisibilityNotify\n"; break;
     case UnmapNotify      : cout << "UnmapNotify\n"; break;
     case GraphicsExpose   : cout << "GraphicsExpose\n"; break;
     default               : cout << "something else\n"; break;
    }

}

void UI::addSimpleText(int _x, int _y, int _w, int _h, string _s , unsigned long _bg, string _s2 /* UIelement parent*/, string _s3 /*text to show*/,bool selectable
                        ,int _bw /*border width*/,unsigned int _bbg /*border background*/ ){
  assert(singleton);  // Assertion `singleton' failed...
  assert(singleton->listOfFrames.find(_s)==singleton->listOfFrames.end()); // if exist, exit the program
  Display *dpy = singleton->getDisplay();

  Window w = XCreateSimpleWindow(dpy,singleton->listOfFrames[_s2].xw,_x,_y,_w,_h,_bw,_bbg,_bg);
  if(selectable) {
    XSelectInput(dpy, w, ExposureMask|ButtonPressMask|KeyReleaseMask|ButtonReleaseMask|SubstructureNotifyMask);
  }else{
    XSelectInput(dpy, w, ExposureMask|KeyReleaseMask|ButtonReleaseMask|SubstructureNotifyMask);
  }
  XMapRaised(dpy,w); // voir la doc française sur les fenêtres
  singleton->listOfSimpleText[w].xw = w;
  singleton->listOfSimpleText[w].forTextArea = _s3;

  singleton->SetFont(&singleton->listOfSimpleText[w],_w,_h);

  singleton->write(&singleton->listOfSimpleText[w]);
}

void UI::SetFont(Element* _el, int _w, int _h){
  int x=4,y=4;
  _el->x=x;
  _el->y=y;
  _el->size = _h;
  Display * dpy = singleton->getDisplay();

  cairo_surface_t *s = cairo_xlib_surface_create(dpy, _el->xw, DefaultVisual(dpy, 0), _w, _h);
  cairo_t *c = cairo_create (s);

/*
  cairo_rectangle(c, 0.0, 0.0, _w, _h);
  cairo_set_source_rgb(c, 1, 0, 0);
  cairo_fill(c);
*/

  _el->c_text = c;
  _el->s_text = s;

  cairo_select_font_face (_el->c_text, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
  cairo_set_font_size (_el->c_text, _h);
  cairo_move_to (_el->c_text, 4.0, _h-_el->y);

}

void UI::SimpleTextSelected(Window *_w){
  cout << "clicked\n";
  if ( singleton->listOfSimpleText.find(*_w)!=singleton->listOfSimpleText.end() ) 
      cout << singleton->listOfSimpleText[*_w].forTextArea << endl;
}

void UI::ElementString(Element *_el, string _s1){
  _el->forTextArea = _s1;
}


void UI::GlyphsTest(int _x, int _y, int _w, int _h, string _s , string _s2 /* UIelement parent*/){
  Display *dpy = singleton->getDisplay();

  Window w = XCreateSimpleWindow(dpy,singleton->listOfFrames[_s2].xw,_x,_y,_w,_h,0,0,0);
  XSelectInput(dpy, w, ExposureMask|ButtonPressMask|KeyReleaseMask|ButtonReleaseMask|SubstructureNotifyMask);
  XMapRaised(dpy,w); // voir la doc française sur les fenêtres

  cairo_surface_t *s = cairo_xlib_surface_create(dpy, w, DefaultVisual(dpy, 0), _w, _h);
  cairo_t *cr = cairo_create (s);
  cairo_select_font_face(cr,"Serif",CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
  cairo_set_font_size(cr,50);


// ce qui suit a été copié depuis "cairo.c" de la fonction "cairo_show_text()"
  int CAIRO_STACK_ARRAY_LENGTH = 512; // chercher dans "cairo-compiler-private.h" pour redefinir ça plus correctement.
  int ARRAY_LENGTH = 512;             // idem

  cairo_status_t status;
  cairo_glyph_t *glyphs, *last_glyph;
  cairo_text_cluster_t *clusters;
  int utf8_len, num_glyphs, num_clusters;
  cairo_text_cluster_flags_t cluster_flags;
  double x=0, y=50; // x0,y0
  cairo_bool_t has_show_text_glyphs;
  cairo_glyph_t stack_glyphs[CAIRO_STACK_ARRAY_LENGTH];
  cairo_text_cluster_t stack_clusters[CAIRO_STACK_ARRAY_LENGTH];
  cairo_scaled_font_t *scaled_font;

  const char *utf8= "This fun€ction first computes a set of glyphs for the string of text. The first glyph is placed so that its origin is at the current point. The origin of each subsequent glyph is offset from that of the previous glyph by the advance values of the previous glyph.";


  scaled_font = cairo_get_scaled_font (cr);
  utf8_len = strlen (utf8);
  has_show_text_glyphs = cairo_surface_has_show_text_glyphs (cairo_get_target (cr));

    glyphs = stack_glyphs;
    num_glyphs = ARRAY_LENGTH; // changement sauvage

    if (has_show_text_glyphs) {
	clusters = stack_clusters;
	num_clusters = ARRAY_LENGTH; // changement sauvage
        cout << "o" << endl;
    } else {
        cout << "n" << endl;
	clusters = NULL;
	num_clusters = 0;
    }

    status = cairo_scaled_font_text_to_glyphs (scaled_font,
					       x, y,
					       utf8, utf8_len,
					       &glyphs, &num_glyphs,
					       has_show_text_glyphs ? &clusters : NULL, &num_clusters,
					       &cluster_flags);


//  cout << "num_glyph=" << num_glyphs << endl;


  if (status != CAIRO_STATUS_SUCCESS){
    cout << "problème dans la création des glyphs" << endl;
    return;    
  }

  int u;
  int decalage_y = 0;
  int decalage_x = 0;
  int correction = 0; // used to shift a char to the origin.
  int largeur = 500;
  int cpt = 0; // ce compteur va compter le nombre de fois que l'on dépasse un nombre supérieur à un multiple de 200
  for(u=0;u<num_glyphs;u++){
//  cout << glyphs[u].x << endl;
    if(glyphs[u].x> (cpt+1)*largeur + x /* think x0 */ ){
      decalage_y+=50;
      decalage_x=-(cpt+1)*largeur;
      cpt++;
      correction = glyphs[u].x +decalage_x; // only computed for the first glyph of a line!
    }
    glyphs[u].y=glyphs[u].y+decalage_y;
    glyphs[u].x=glyphs[u].x+decalage_x-correction;
  }


  cairo_set_source_rgb(cr, 1, 0, 0); // blééééérooooo!!
  cairo_show_glyphs(cr,glyphs, num_glyphs);


  if (glyphs != stack_glyphs) cairo_glyph_free (glyphs);
  if (clusters != stack_clusters) cairo_text_cluster_free (clusters);
}

void UI::addGlyphInput(int _x, int _y, int _w, int _h, string _s /*name*/, string _s2 /*parent name*/, string _s3 /*something to show*/){
  Display *dpy = singleton->getDisplay();

  Window w = XCreateSimpleWindow(dpy,singleton->listOfFrames[_s2].xw,_x,_y,_w,_h,0,0,0);
  XSelectInput(dpy, w, ExposureMask|ButtonPressMask|KeyReleaseMask|ButtonReleaseMask|SubstructureNotifyMask);
  XMapRaised(dpy,w); // voir la doc française sur les fenêtres

  cairo_surface_t *s = cairo_xlib_surface_create(dpy, w, DefaultVisual(dpy, 0), _w, _h);
  cairo_t *cr = cairo_create (s);
  cairo_select_font_face(cr,"Serif",CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
  cairo_set_font_size(cr,50);


// ce qui suit a été copié depuis "cairo.c" de la fonction "cairo_show_text()"
  int CAIRO_STACK_ARRAY_LENGTH = 512; // chercher dans "cairo-compiler-private.h" pour redefinir ça plus correctement.
  int ARRAY_LENGTH = 512;             // idem

  cairo_status_t status;
  cairo_glyph_t *glyphs, *last_glyph;
  cairo_text_cluster_t *clusters;
  int utf8_len, num_glyphs, num_clusters;
  cairo_text_cluster_flags_t cluster_flags;
  double x=0, y=50; // x0,y0
  cairo_bool_t has_show_text_glyphs;
  cairo_glyph_t stack_glyphs[CAIRO_STACK_ARRAY_LENGTH];
  cairo_text_cluster_t stack_clusters[CAIRO_STACK_ARRAY_LENGTH];
  cairo_scaled_font_t *scaled_font;

  const char *utf8= _s3.c_str();


  scaled_font = cairo_get_scaled_font (cr);
  utf8_len = strlen (utf8);
  has_show_text_glyphs = cairo_surface_has_show_text_glyphs (cairo_get_target (cr));

    glyphs = stack_glyphs;
    num_glyphs = ARRAY_LENGTH; // changement sauvage

    if (has_show_text_glyphs) {
	clusters = stack_clusters;
	num_clusters = ARRAY_LENGTH; // changement sauvage
        cout << "o" << endl;
    } else {
        cout << "n" << endl;
	clusters = NULL;
	num_clusters = 0;
    }

    status = cairo_scaled_font_text_to_glyphs (scaled_font,
					       x, y,
					       utf8, utf8_len,
					       &glyphs, &num_glyphs,
					       has_show_text_glyphs ? &clusters : NULL, &num_clusters,
					       &cluster_flags);


//  cout << "num_glyph=" << num_glyphs << endl;


  if (status != CAIRO_STATUS_SUCCESS){
    cout << "problème dans la création des glyphs" << endl;
    return;    
  }

  int u;
  int decalage_y = 0;
  int decalage_x = 0;
  int correction = 0; // used to shift a char to the origin.
  int largeur = 500;
  int cpt = 0; // ce compteur va compter le nombre de fois que l'on dépasse un nombre supérieur à un multiple de 200
  for(u=0;u<num_glyphs;u++){
//  cout << glyphs[u].x << endl;
    if(glyphs[u].x> (cpt+1)*largeur + x /* think x0 */ ){
      decalage_y+=50;
      decalage_x=-(cpt+1)*largeur;
      cpt++;
      correction = glyphs[u].x +decalage_x; // only computed for the first glyph of a line!
    }
    glyphs[u].y=glyphs[u].y+decalage_y;
    glyphs[u].x=glyphs[u].x+decalage_x-correction;
  }


  cairo_set_source_rgb(cr, 1, 0, 0); // blééééérooooo!!
  cairo_show_glyphs(cr,glyphs, num_glyphs);


  if (glyphs != stack_glyphs) cairo_glyph_free (glyphs);
  if (clusters != stack_clusters) cairo_text_cluster_free (clusters);
}

void UI::redrawWindow(XEvent *_e){
  Display *dpy = singleton->getDisplay();
  GC g = XCreateGC (dpy, _e->xany.window, 0, NULL);
 // cout << "redraw\n";
  if(singleton->listOfTextAreas.find(_e->xany.window)!=singleton->listOfTextAreas.end() ){
    cout << "x:" <<_e->xexpose.x << " y:" << _e->xexpose.y << " w:" << _e->xexpose.width << " h:" << _e->xexpose.height << endl;
    XCopyArea(dpy, singleton->listOfTextAreas[_e->xany.window].double_buffer, _e->xany.window, g, _e->xexpose.x, _e->xexpose.y, _e->xexpose.width, _e->xexpose.height, _e->xexpose.x, _e->xexpose.y);
    return;
  }

  if(singleton->listOfButtons.find(_e->xany.window)!=singleton->listOfButtons.end() ){
    XCopyArea(dpy, singleton->listOfButtons[_e->xany.window].double_buffer, _e->xany.window, g, _e->xexpose.x, _e->xexpose.y, _e->xexpose.width, _e->xexpose.height, _e->xexpose.x, _e->xexpose.y);
    return;
  }

/*
  XWindowAttributes wa;
  XGetWindowAttributes(dpy, *_w, &wa);
  GC g = XCreateGC (dpy, *_w, 0, NULL);
  if(singleton->listOfButtons.find(*_w)!=singleton->listOfButtons.end() )
    XCopyArea(dpy, singleton->listOfButtons[*_w].double_buffer, *_w, g, 0, 0, wa.width, wa.height, 0, 0);
  if(singleton->listOfTextAreas.find(*_w)!=singleton->listOfTextAreas.end() )
    XCopyArea(dpy, singleton->listOfTextAreas[*_w].double_buffer, *_w, g, 0, 0, wa.width, wa.height, 0, 0);
  if(singleton->listOfSimpleText.find(*_w)!=singleton->listOfSimpleText.end() )
    XCopyArea(dpy, singleton->listOfSimpleText[*_w].double_buffer, *_w, g, 0, 0, wa.width, wa.height, 0, 0);
*/

}

void UI::saveWindow(XEvent *_e){
  cout << "saveWindow() called" << endl;
// cout << "  x=" << _e->xgraphicsexpose.x << endl;
  Display *dpy = singleton->getDisplay();
  GC g = XCreateGC (dpy, _e->xany.window, 0, NULL);
  XWindowAttributes wa;
//  XGetWindowAttributes(dpy, _e->xany.window, &wa);

  if(singleton->listOfTextAreas.find(_e->xany.window)!=singleton->listOfTextAreas.end() ){
    XCopyArea(dpy, _e->xany.window, singleton->listOfTextAreas[_e->xany.window].double_buffer, g, 0, 0, singleton->listOfTextAreas[_e->xany.window].width, singleton->listOfTextAreas[_e->xany.window].height, 0, 0);
  }

}

////////////////////////////////////////////////////////////// INTERNAL /////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// INTERNAL /////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// INTERNAL /////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// INTERNAL /////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// INTERNAL /////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////// INTERNAL /////////////////////////////////////////////////////////

/** 
 * log() (internal)
 * @_s: message to record in the log.
 *
 * this method is used to record a message in the log.
 **/
#ifdef LOG
void UI::log(string _s) {
  this->logRainOfAcid <<"[" << this->timer() << "] "<< _s << endl;
}
#endif

/** 
 * log() (internal)
 * @_s: message to record in the log.
 * @_l: line of the code to refer.
 * @_f: file to refer.
 *
 * this method is used to record a message in the log.
 **/
#ifdef LOG
void UI::log(string _s, int _l, string _f) {
  this->logRainOfAcid <<"[" << this->timer() << "] "<< _s << " at line (" << _l << ") in [" << _f <<"]" << endl;

}
#endif


/**
 * timer() (internal)
 *
 * this method return the current time in a string
 **/
#ifdef LOG
string UI::timer() {
  // Declarations
  time_t     t;
  struct tm  *lt;
  char       buffer[9];

  time(&t);
  lt = localtime(&t);
  strftime(buffer, 9, "%T", lt);
  return string(buffer);

}
#endif

/**
 * abort() (internal)
 *
 * this method is used to abort the program
 **/
void UI::abort() {
  singleton->aborting = true;
  #ifdef LOG
  singleton->log("Aborting sequence initiated!");
  #endif
  singleton->kill();
}

/**
 * checkSingleton() (external)
 * @_b: has to be set to true if singleton should exists, otherwise set to false.
 * @_l: number of the line from this function is called (use __LINE__ )
 * @_f: name of the file from this function is called (use __FILE__ )
 *
 * this method check if the singleton exists, otherwise the program will exit. This method need to be called even if the singleton
 * not exists, so it can be an internal function. In some other words, one can call this function too.
 **/
void UI::checkSingleton(bool _b, int _l, string _f)
{
  if ( !singleton && _b ){ // we are looking to know if singleton exists
    cout << "(EE) singleton not initiated. At line(" << _l << ") in (" << _f <<")" << endl;
    exit(-1);
  }

  if ( singleton && !_b ){ // we are looking to know if singleton doesn't exists
    cout << "(EE) singleton is initiated but it shouldn't. At line(" << _l << ") in (" << _f <<")" << endl;
    exit(-1);
  }
}