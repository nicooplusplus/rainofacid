Licence :
=========
  Copyright 2014 Jäger Nicolas
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA 02110-1301, USA.



Overview :
==========
  ***«Rain Of Acid»*** is a X11 toolkit. My aim is to build a toolkit
  through few classes with few dependencies. The aim is **NOT** to do
  a gtk-like.
 
Features :
==========
  - Frame, button, text input, log

Dependecies :
==============
  - X11
  - Xft
  - FreeType2
  - Pthread
  - Cairomm (cairo binding for c++)

Compil :
========
  - $ cmake .
  - $ make

TODO : (sry mostly in french yet...)
======
  - check if the example is started as root.
  - gestion des évènements plus complet
  - mieux définir les mask (xlib) pour les différements éléments "UIelement"
  - reserver le mot "rootwin"
  - "UI::addFrame" ne semble pas vérifier si le parent existe!
  - faire en sorte que le bouton peut être dimensionné automatiquement en fonction de la longueur du texte.
  - faire que un clique soit "presser"+"relacher".
  - faire un template pour "frame" et "text area"
  - verifier si le parent existe pour chaque "text area" à la création.
  - verifier si le boutton existe déjà. Pour l'instant le problème c'est que l'on a la liste qui est trié par "Window"
    or dans le cas présent en oblige que chaque fenêtre est un titre différent, donc il faut pouvoir vérifier par titre.
  - spécifier la commande en fonction du bouton de souris relâché, car actuellement il n'y a pas de discrimination.
  - vérifier les destructeurs pour cairo!
  - sanity check for input manager, if not inisalised or whatever... also check for his deathtruction
  - revoir qui doit être "static" et qui ne doit pas l'être.
  - dans "UI::addTextInput" il faut sélectionner un "XIMStyle" pour le "IC", encore faut-il connaître les différences.
  - besoin d'écrire du code pour verifier la sanité d'allouer de l'espace pour un "vector"
  - remplacer le "string" par "window" dans "listOfFrame"
  - "SetFont()" est appelé pour chaque element ayant besoin de texte, i.e. par "addSimpleText" et "addTextArea" ne doit pas 
    être appelée par l'utilisateur, en écrire une autre pour ça!
  - TODO : mettre à jour "Element" : ajouter x, y et tout ce qui est lié à l'élement (h,w,something else ?). 
  - sélection de texte avec la souris...
  - il faut demander ce que "CAIRO_STACK_ARRAY_LENGTH" est au juste dans "cairo" car le fait d'avoir des parantahèses il
    semblerait que cela soit une fonction (une macro peut être ?).
  - il me semblerait plus logique de passer l'"event" en paramètre des fonctions appellées depuis la boucle d'évènements que la
    fenêtre qui lui est associée, premièrement parce que ça permet de mieux les identifiers, deuxièmement cela homogénéise leur
    écriture. Enfin dans certains cas on peut avoir besoin d'informations comme dans le cas "noExpose".
  - il faut trouver un moyen de definir «#define LOG» dans le main!
  - retirer #01# quand l'écriture de addframe aura été réalisée, présentement on en a besoin car l'identification de la fenêtre
    root se fait via une "string" (rootwin);
 

Known bugs :
============
  - none! (yet...)
  
About the name :
================
  *«Rain Of Acid»* comes after a metal band :
  http://www.metal-archives.com/bands/Rain_of_Acid
  This name was set randomly, through **random band**
